#!/usr/bin/env python

from __future__ import print_function

import io
import time
#import picamera
import cv2

"""
This quick test gives an idea of your capture speed using opencv with a 
USB camera. This test will work on Linux (and Raspberry PI) with a USB
canera. 3656 has used the Raspberry PI camera in previous years but have
found a good USB camera like the MS Life HD150 Camera a little bit more
reliable and durable.
"""


class Timer(object):
    def __init__(self, name):
        self.name = name
        self.t1 = time.time()
        pass

    def start(self):
        self.t1 = time.time()

    def stop(self):
        self.t2 = time.time()

    def elapsed(self):
        return self.t2-self.t1

    def show(self):
        print( "%s: %4.4f" % ( self.name, (self.t2-self.t1) ) )
#|

from imutils.video import VideoStream

"""
Demonstrates continous image capture technique suggested here:
 - http://www.pyimagesearch.com/2015/03/30/accessing-the-raspberry-pi-camera-with-opencv-and-python/

"""

t = Timer( "Camera continous" )
idx=0
cam = VideoStream(src=0).start()

t.start()
#cam = cv2.VideoCapture(0)
#stream = io.BytesIO()

for idx in range(20):
    img = cam.read()

t.stop()

cv2.imshow( 'frame', img )
cv2.waitKey(0)



print( "%s iterations of image captures" % idx )

secs = (t.t2 - t.t1)
fps = 1.0 * idx / secs

t.show()

print( "fps = %.2f" % (fps) )


