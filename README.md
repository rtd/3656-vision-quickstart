# Quick Start Project for FRC Python Vision Primitives

This project serves to illustrate the fundamentals of implementing
a Python Robotic Vision solution for FRC. The intent is to familarize
beginners with the basics of implementing FRC vision.

The code was prepared to presented with this PyOhio 2018 presentation:

- http://bit.ly/3656PyOhio2018

If you find this useful please send an email to:

sewardrobert@gmail.com

Suggestions, constructive comments and advice are always welcome.

## Installing OpenCV

If you have OpenCV installed on your PI this code should just work. For other OSes if you google around, you should be able to find instructions for your OS. Expect to spend 5 to 60 minutes installing OpenCV binaries depending on your environment. Although OpenCV may be a slight challenge to install, the pay off is worth it I think! It is quite fun to work with.

On Fedora 28, I installed opencv like so:

  - dnf install python3-opencv
  - mkdir pyve
  - virtualenv --python=/usr/bin/python3 --system-site-packages pyve
  - . pyve/bin/activate
  - pip install -r requirements.txt
  

If you would like a great introductory text on Python and OpenCV please consider this book:
 - http://www.pyimagesearch.com/practical-python-opencv/

